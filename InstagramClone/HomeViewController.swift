//
//  HomeViewController.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 12/4/16.
//  Copyright © 2016 The Zero2Launch Team. All rights reserved.
//
    
import UIKit
import SDWebImage
import ImagePicker
import DeallocationChecker

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    fileprivate var isLoadingPost = false
    let refreshControl = UIRefreshControl()
    
    var posts = [Post]()
    var users = [UserModel]()
    var allToBlock = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 521
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        tableView.delegate = self
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl
        activityIndicatorView.startAnimating()
        
//        Api.Block.allUserBlockedBy(completed: {
//            allUserBlockedBy in
//            print("All user Blocked By: \(allUserBlockedBy)")
//            Api.Block.allUserBlockings(completed: {
//                allUserBlockings in
//                print("All user Blockings: \(allUserBlockings)")
//                self.allToBlock = Array(Set(allUserBlockings + allUserBlockedBy))
//                
//                self.loadAllPosts(allToBlock: self.allToBlock)
//            })
//        })
        
        Api.Block.allUserBlockings(completed: {
            allUserBlockings in
            print("All user Blockings: \(allUserBlockings)")
            
            Api.Block.allUserBlockedBy(completed: {
                allUserBlockedBy in
                print("All user Blocked By: \(allUserBlockedBy)")
                
                self.allToBlock = Array(Set(allUserBlockings + allUserBlockedBy))
                
                self.loadAllPosts(allToBlock: self.allToBlock)
                
            })
            
        })
        
//        allUserBlockings(completed: { value in
//            print("All Blockings Value: \(value)")
//            self.loadPosts()
//        })
        //loadPosts()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        DeallocationChecker.shared.checkDeallocation(of: self)
    }
    
    @objc func refresh() {
        posts.removeAll()
        users.removeAll()
        //loadPosts()
        print("All to block refresh(): \(allToBlock)")
        loadAllPosts(allToBlock: allToBlock)
    }
    
    func loadAllPosts(allToBlock: [String]){
        print("loadAllPosts called")
        
        print("All to block loadAllPosts(): \(allToBlock)")
        isLoadingPost = true

        self.posts.removeAll()
        self.users.removeAll()
        self.tableView.reloadData()
        
        Api.Post.observeRecentPostsAsArray(completion: {
            posts in

            Api.User.observeUsersAsArray(withPosts: posts, completion: {
                profiles in

                var sortedProfiles = [UserModel]()
                for post in posts {
                    self.posts.append(post)
                    let userId = post.authorId!
                    
                    if let user = profiles.first(where: { $0.id == userId }) {
                        sortedProfiles.append(user)
                    }
                }
                self.users = sortedProfiles
                
                self.posts.removeAll(where: {allToBlock.contains($0.authorId!)})
                self.users.removeAll(where: {allToBlock.contains($0.id!)})
                
                self.tableView.reloadData()
            })

            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            self.isLoadingPost = false
            self.activityIndicatorView.stopAnimating()

        })
        
    }
    
    func loadPosts() {
        print("loadPosts called")
        isLoadingPost = true

        self.posts.removeAll()
        self.users.removeAll()
        self.tableView.reloadData()
        

        // Include blocking profiles/users:
        // 1) don't show people and posts that have been blocked by the user
        // 2) don't show people that have blocked the user

        Api.Post.observeRecentPostsAsArray(completion: {
            posts in
            //self.posts = posts

            Api.User.observeUsersAsArray(withPosts: posts, completion: {
                profiles in

                var sortedProfiles = [UserModel]()
                for post in posts {
                    self.posts.append(post)
                    let userId = post.authorId!
                    
                    if let user = profiles.first(where: { $0.id == userId }) {
                        sortedProfiles.append(user)
                    }

                    // In order to block, need to pass the posts and profiles to another
                    // method to ensure that the callback to isBlocking is returned before continuing
                    
//                    self.isBlockingOrBlocked(userId: userId, completed: {
//                        bool in
//
//                        if bool {
//                            // user is blocked or blocking
//                            print("User is blocked or blocking")
//                            self.users.removeAll(where: { $0.id == userId })
//                            self.posts.removeAll(where: {$0.authorId == userId })
//
//                            self.tableView.reloadData()
//                        } else {
//                            print("User is Free")
//                            self.tableView.reloadData()
//                        }
//                    })
                }
                self.users = sortedProfiles
                self.tableView.reloadData()
            })

            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            self.isLoadingPost = false
            self.activityIndicatorView.stopAnimating()

        })
        
        

    }
    
    
    func isBlocking(userId: String, completed: @escaping (Bool) -> Void) {
        Api.Block.isBlocking(userId: userId, completed: completed)
    }
    
    func isBlocked(userId: String, completed: @escaping (Bool) -> Void) {
        Api.Block.isBlockedBy(userId: userId, completed: completed)
    }
    
    func isBlockingOrBlocked(userId: String, completed: @escaping (Bool) -> Void) {
        var isBlocking = false
        var isBlocked = false
        Api.Block.isBlocking(userId: userId) { Bool in
            isBlocking = Bool
            Api.Block.isBlockedBy(userId: userId) { Bool in
                isBlocked = Bool
                
                if isBlocking || isBlocked {
                    completed(true)
                } else {
                    completed(false)
                }
            }
        }
    }
    
    func allUserBlockings(completed: @escaping ([String]) -> Void) {
        
        Api.Block.allUserBlockings(completed: completed)
    
    }
    
    private func displayNewPosts(newPosts posts: [Post]) {
        guard posts.count > 0 else {
            return
        }        
        var indexPaths:[IndexPath] = []
        self.tableView.beginUpdates()
        for post in 0...(posts.count - 1) {
            let indexPath = IndexPath(row: post, section: 0)
            indexPaths.append(indexPath)
        }
        self.tableView.insertRows(at: indexPaths, with: .none)
        self.tableView.endUpdates()
    }
    
    
    @IBAction func dmBtnDidTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Messages", bundle: nil)
        let dmViewController = storyboard.instantiateViewController(withIdentifier: "MessagesViewControllerID") as! MessagesTableViewController
        self.navigationController?.pushViewController(dmViewController, animated: true)
            
    }
    
    @IBAction func mapBtnDidTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Map", bundle: nil)
        let mapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewControllerID") as! MapViewController
        
    
        self.navigationController?.pushViewController(mapViewController, animated: true)
    }
    
    
//    @IBAction func cameraBtnDidTapped(_ sender: Any) {
//        let imagePickerController = ImagePickerController()
//        imagePickerController.delegate = self
//        imagePickerController.imageLimit = 1
//        present(imagePickerController, animated: true, completion: nil)
//    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard let profileImg = images.first else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        ProgressHUD.show("Waiting...", interaction: false)
        if let imageData = profileImg.jpegData(compressionQuality: 0.1) {
            let ratio = profileImg.size.width / profileImg.size.height
            HelperService.uploadDataToServer(data: imageData, videoUrl: nil, ratio: ratio, caption: "", title: "", onSuccess: {
                self.dismiss(animated: true, completion: nil)
                self.posts.removeAll()
                self.users.removeAll()
                self.loadPosts()
            })
            
        } else {
            ProgressHUD.showError("Profile Image can't be empty")
        }
        
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        print("cancel")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CommentSegue" {
            let commentVC = segue.destination as! CommentViewController
            let postId = sender  as! String
            commentVC.postId = postId
        }
        
        if segue.identifier == "Home_ProfileSegue" {
            let profileVC = segue.destination as! ProfileUserViewController
            let userId = sender  as! String
            profileVC.userId = userId
        }
        
        if segue.identifier == "Home_HashTagSegue" {
            let hashTagVC = segue.destination as! HashTagViewController
            let tag = sender  as! String
            hashTagVC.tag = tag
        }
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if posts.isEmpty {
            return 0
        }
        print("Posts Count:")
        print(posts.count)
        return posts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! HomeTableViewCell
        if posts.isEmpty {
            return UITableViewCell()
        }
        let post = posts[indexPath.row]
        
        cell.post = post
        
        if indexPath.row >= users.startIndex && indexPath.row < users.endIndex {
            let user = users[indexPath.row]
            cell.user = user
        }
        
        cell.delegate = self
        return cell
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
           
            guard !isLoadingPost else {
                return
            }
            isLoadingPost = true

            guard let lastPostTimestamp = self.posts.last?.createdDate else {
                isLoadingPost = false
                return
            }
            Api.Feed.getOldFeed(withId: Api.User.CURRENT_USER!.uid, start: lastPostTimestamp, limit: 5) { (results) in
                if results.count == 0 {
                    return
                }
                for result in results {
                    self.posts.append(result.0)
                    self.users.append(result.1)
                }
                self.tableView.reloadData()

                self.isLoadingPost = false
            }

        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name.init("stopVideo"), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: NSNotification.Name.init("playVideo"), object: nil)

    }
}

extension HomeViewController: HomeTableViewCellDelegate {
    
    func goToCommentVC(postId: String) {
        performSegue(withIdentifier: "CommentSegue", sender: postId)
    }
    
    func goToProfileUserVC(userId: String) {
        performSegue(withIdentifier: "Home_ProfileSegue", sender: userId)
    }
    
    func goToHashTag(tag: String) {
        performSegue(withIdentifier: "Home_HashTagSegue", sender: tag)
    }
}



//              old loadPosts()

//        Api.Feed.getRecentFeed(withId: Api.User.CURRENT_USER!.uid, start: posts.first?.createdDate, limit: 3  ) { (results) in
//            if results.count > 0 {
//                results.forEach({ (result) in
//                    self.posts.append(result.0)
//                    self.users.append(result.1)
//                })
//            }
//            if self.refreshControl.isRefreshing {
//                self.refreshControl.endRefreshing()
//            }
//            self.isLoadingPost = false
//
//            self.activityIndicatorView.stopAnimating()
//            self.tableView.reloadData()
//        }
//

//        Api.Post.observeRecentPosts { (post) in
//            Api.User.observeUser(withId: post.authorId!, completion: {
//                user in
//                self.posts.append(post)
//                self.users.append(user)
//                self.tableView.reloadData()
//            })
//            if self.refreshControl.isRefreshing {
//                self.refreshControl.endRefreshing()
//            }
//            self.isLoadingPost = false
//            self.activityIndicatorView.stopAnimating()
//        }
        


//        Api.Post.observeRecentPostsAsArray(completion: {
//            posts in
//            self.posts = posts
//
//            Api.User.observeUsersAsArray(withPosts: posts, completion: {
//                profiles in
//                print("profiles:")
//                print(profiles)
//                var sortedProfiles = [UserModel]()
//                for post in posts {
//                    let userId = post.authorId!
//
//                    if let user = profiles.first(where: { $0.id == userId }) {
//                        print(user.id)
//                        sortedProfiles.append(user)
//                    }
//                }
//                print("sorted profiles:")
//                print(sortedProfiles)
//                self.users = sortedProfiles
//                self.tableView.reloadData()
//            })
//
//            if self.refreshControl.isRefreshing {
//                self.refreshControl.endRefreshing()
//            }
//            self.isLoadingPost = false
//            self.activityIndicatorView.stopAnimating()
//        })
