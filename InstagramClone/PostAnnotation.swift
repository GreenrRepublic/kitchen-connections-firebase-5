//
//  PostAnnotation.swift
//  KitchenConnectionsFB5
//
//  Created by Captain on 2019-09-02.
//  Copyright © 2019 The Zero2Launch Team. All rights reserved.
//

import MapKit

class PostAnnotation: NSObject, MKAnnotation {
    
    // This property must be key-value observable, which the `@objc dynamic` attributes provide.
    @objc dynamic var coordinate = CLLocationCoordinate2D(latitude: 45.1339, longitude: -75.4119)
    
    var title: String? = "Testing the Thingy"
    var subtitle: String? = "Test Subtitle."
    var photoURL: String? = ""
    var postId: String? = ""
    
    
    func setImageUrl(imageURL: String){
        self.photoURL = imageURL
    }
    
}
