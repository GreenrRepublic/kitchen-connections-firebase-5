//
//  AppDelegate.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 11/28/16.
//  Copyright © 2016 The Zero2Launch Team. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import SDWebImage
import CoreLocation
import DeallocationChecker
import GoogleSignIn
import AuthenticationServices

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?
    var databaseRef : DatabaseReference!
    let locationManager = CLLocationManager()

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error);
            return
        }
        print("user signed in to google");
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                print(error);
                return
            }
            // User is signed in
            // ...
            print("user signed in to firebase")
            
            // checking database ref
            
            self.databaseRef = Database.database().reference()
            self.databaseRef.child("profiles").child(user!.user.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                let snapshot = snapshot.value as? NSDictionary
                
                if(snapshot == nil){
                    
                    //user logging in for the first time
                    self.databaseRef.child("profiles").child(user!.user.uid).child("username").setValue(user?.user.displayName)
                    self.databaseRef.child("profiles").child(user!.user.uid).child("email").setValue(user?.user.email)
                    self.databaseRef.child("profiles").child(user!.user.uid).child("photoUrl").setValue(user?.user.photoURL?.absoluteString)
                    self.databaseRef.child("profiles").child(user!.user.uid).child("id").setValue(user?.user.uid)
                    self.databaseRef.child("profiles").child(user!.user.uid).child("username_lowercase").setValue(user?.user.displayName?.lowercased())
                    
                }
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                self.window?.rootViewController?.performSegue(withIdentifier: "signInToTabbarVC", sender: nil)
                
                
            })
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance()!.handle(url)
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
//        SDImageCache.shared.config.maxDiskAge = 3600 * 24 * 7 //1 Week
//        SDImageCache.shared.config.maxMemoryCost = 1024 * 1024 * 20 //Aprox 20 images
//        SDImageCache.shared.config.shouldCacheImagesInMemory = false //Default True => Store images in RAM cache for Fast performance.
//        //SDImageCache.shared.config.shouldDecompressImages = false
//        //SDWebImageDownloaderOptions.avoidDecodeImage = true
//        //print("rawValue")
//        SDImageCache.shared.config.diskCacheReadingOptions = NSData.ReadingOptions.mappedIfSafe
//        SDWebImageDownloader.shared.config.maxConcurrentDownloads = 4
//        
        //SDImageCacheOptions.avoidDecodeImage
 
        //SDImageCache.shared().config.maxCacheAge = 3600 * 24 * 7 //1 Week
        SDImageCache.shared()?.maxCacheAge = 3600 * 24 * 7 //1 Week
        SDImageCache.shared().maxMemoryCost = 1024 * 1024 * 20 //Aprox 20 images
        //SDImageCache.shared().config.shouldCacheImagesInMemory = false //Default True => Store images in RAM cache for Fast performance
        SDImageCache.shared()?.shouldCacheImagesInMemory = false //Default True => Store images in RAM cache for Fast performance
        //SDImageCache.shared().config.shouldDecompressImages = false
        SDImageCache.shared()?.shouldDecompressImages = false
        SDWebImageDownloader.shared().shouldDecompressImages = false
//        SDImageCache.shared().config.diskCacheReadingOptions = NSData.ReadingOptions.mappedIfSafe

        
        
        // Override point for customization after application launch.
        // change color of tab bar's items
        UITabBar.appearance().tintColor = .black
        FirebaseApp.configure()
        
        locationManager.delegate = self
        enableLocationServices()
        
        let isRunningUnderUITests = ProcessInfo.processInfo.environment["uitests"] != nil

//        #if DEBUG
//        if isRunningUnderUITests {
//            DeallocationChecker.shared.setup(with: .callback(makeUITestsCallback()))
//        } else {
//            DeallocationChecker.shared.setup(with: .alert)
//        }
//        #endif
        
        // Initialize Google Sign-In
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        
        
        return true
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func enableLocationServices(){
        
        //locationManager.delegate = self
        
        switch CLLocationManager.authorizationStatus() {
            
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            print(".notDetermined")
            break
            
        case .restricted, .denied:
            // Disable location features
            
            //disableMyLocationBasedFeatures()
            print(".restricted/denied")
            // TODO: Redirect user to home
            
            break
            
        case .authorizedWhenInUse:
            // Enable basic location features
            //enableMyWhenInUseFeatures()
            locationManager.requestLocation()
            print(".authorizedWhenInUse")
            break
            
        case .authorizedAlways:
            // Enable any of your app's location features
            //enableMyAlwaysFeatures()
            print("authorizedAlways")
            break
        }
    }
    
    private func checkForPreviousAppleLogin() {
        let id = Utilities.fetchStringFromDefaults(key: Constant.apple_sign_in_id)
        
        ASAuthorizationAppleIDProvider().getCredentialState(forUserID: id, completion: { credentialState, error in
            
            switch(credentialState){
            case .authorized:
                print("user remain logged in, proceed to another view")
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                self.window?.rootViewController?.performSegue(withIdentifier: "signInToTabbarVC", sender: nil)
                
            case .revoked:
                print("user logged in before but revoked")
            case .notFound:
                print("user haven't log in before")
            default:
                print("unknown state")
            }
            
        })
        
    }

}

extension AppDelegate: CLLocationManagerDelegate {
    
    // Handle getting location data
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("didUpdateLocations")
        if let location = locations.last {
            //    let center = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            
            print(location.coordinate.latitude)
            let preferences = UserDefaults.standard
            let latKey = "locationLat"
            let lonKey = "locationLon"
            let locKey = "location"
            preferences.set("\(location.coordinate.latitude)", forKey: latKey)
            preferences.set("\(location.coordinate.longitude)", forKey: lonKey)
            
            let geocoder = CLGeocoder()
            
            geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
                if error == nil {
                    let firstLocation = placemarks?[0]
                    let areaOfInterest = firstLocation?.areasOfInterest
                    if areaOfInterest != nil {
                        preferences.set(areaOfInterest, forKey: locKey)
                        print(areaOfInterest)
                    } else {
                        let country = firstLocation?.country
                        let city = firstLocation?.locality
                        let province = firstLocation?.administrativeArea
                        let addy = "\(city!), \(province!), \(country!)"
                        preferences.set(addy, forKey: locKey)
                        print(addy)
                    }
                    
                    //print(firstLocation)
                }
                else {
                    // An error occurred during geocoding.
                    print(error)
                }
            })
        }
        
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("error: \(error.localizedDescription)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
            
        case .restricted, .denied:
            // Disable your app's location features
            //disableMyLocationBasedFeatures()
            print("locationManager() called - restricted or denied")
            print(status.rawValue)
            break
            
        case .authorizedWhenInUse:
            // Enable only your app's when-in-use features.
            //enableMyWhenInUseFeatures()
            print("locationManager() called - authorized when in use")
            print(status.rawValue)
            locationManager.requestLocation()
            break
            
        case .authorizedAlways:
            // Enable any of your app's location services.
            //enableMyAlwaysFeatures()
            print("locationManager() called - authorized always")
            print(status.rawValue)
            locationManager.requestLocation()
            break
            
        case .notDetermined:
            print("locationManager() called - not determined")
            print(status.rawValue)
            break
        }
    }
    
    private func makeUITestsCallback() -> DeallocationChecker.Callback {
        return { leakState, _ in
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = UIViewController()
            window.makeKeyAndVisible()
            
            let message: String
            switch leakState {
            case .leaked:
                message = "leaked"
            case .notLeaked:
                message = "notLeaked"
            }
            
            let alertController = UIAlertController(title: "Leak Status", message: message, preferredStyle: .alert)
            alertController.addAction(.init(title: "OK", style: .cancel, handler: nil))
            
            window.rootViewController?.present(alertController, animated: false, completion: nil)
        }
    }
    
}

