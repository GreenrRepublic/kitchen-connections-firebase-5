//
//  Constant.swift
//  KitchenConnectionsFB5
//
//  Created by Garret Kielburger on 2021-09-07.
//  Copyright © 2021 The Zero2Launch Team. All rights reserved.
//

import Foundation

public enum Constant {
    
    static let eula = "EULA"
    static let apple_sign_in_id = "APPLE_ID"
    
}
