//
//  InboxApi.swift
//  Kitchen Connections
//
//  Created by Garret Kielburger on 2020-08-13.
//  Copyright © 2020 Greenr Republic. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

typealias InboxCompletion = (Inbox) -> Void


class InboxApi {
    func lastMessages(id: String, onSuccess: @escaping(InboxCompletion) ) {
        print("lastMessages called")
        let ref = Database.database().reference().child(REF_INBOX).child(id)
        // should limit to last 8 conversations?
        ref.queryOrdered(byChild: "date").queryLimited(toLast: 8).observe(DataEventType.childAdded) { (snapshot) in
            if let dict = snapshot.value as? Dictionary<String, Any> {
             //  print("dictionary from fb: ")
             //   print(dict)
                
                guard let partnerId = dict["to"] as? String else {
                    return
                }
        let id = (partnerId == Api.User.currentUserId) ? (dict["from"] as! String) : partnerId
                
                // weird thing is that id and partnerId are the same!?!
                let channelId = Message.md5Hash(forMembers: [id, partnerId])

                Api.User.getUserInfor(id: id, onSuccess: { (user) in
                    
                    if let inbox = Inbox.transformInbox(dict: dict, channel: channelId, user: user) {

                        onSuccess(inbox)
                    }
                })
                
        print("Id: ")
        print(id)
            }
        }
        
    }
    
    func loadMore(start timestamp: Double?, controller: MessagesTableViewController, from: String, onSuccess: @escaping(InboxCompletion)) {
        guard let timestamp = timestamp else {
            return
        }
        let ref = Database.database().reference().child(REF_INBOX).child(from).queryOrdered(byChild: "date").queryEnding(atValue: timestamp - 1).queryLimited(toLast: 3)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let allObjects = snapshot.children.allObjects as? [DataSnapshot] else {
                return
            }
            if allObjects.isEmpty {
                controller.tableView.tableFooterView = UIView()
            }
            
            allObjects.forEach({ (object) in
                if let dict = object.value as? Dictionary<String, Any> {
                    guard let partnerId = dict["to"] as? String else {
                        return
                    }
                    //let channelId = Message.hash(forMembers: [from, partnerId])
                    let channelId = Message.md5Hash(forMembers: [from, partnerId])
                    Api.User.getUserInfor(id: partnerId, onSuccess: { (user) in
                        if let inbox = Inbox.transformInbox(dict: dict, channel: channelId, user: user) {
                            onSuccess(inbox)
                        }
                    })
                }
            })
        }
    }
    
}
