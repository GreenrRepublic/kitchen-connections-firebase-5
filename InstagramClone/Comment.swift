//
//  Comment.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 1/8/17.
//  Copyright © 2017 The Zero2Launch Team. All rights reserved.
//

import Foundation
class Comment {
    var id: String?
    var text: String?
    var authorId: String?
    var createdDate: CLong?
    
}

extension Comment {
    static func transformComment(dict: [String: Any]) -> Comment {
        let comment = Comment()
        comment.text = dict["text"] as? String
        comment.id = dict["id"] as? String
        comment.authorId = dict["authorId"] as? String
        comment.createdDate = dict["createdDate"] as? CLong
        return comment
    }
}
