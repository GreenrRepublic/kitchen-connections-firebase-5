//
//  Utilities.swift
//  KitchenConnectionsFB5
//
//  Created by Garret Kielburger on 2021-09-07.
//  Copyright © 2021 The Zero2Launch Team. All rights reserved.
//

import Foundation


public struct Utilities {

    static func saveEULA(bool: Bool) {
        saveBooleanInDefaults(value: bool, key: Constant.eula)
    }
    
    static func fetchEULA() -> Bool {
        fetchBooleanFromDefaults(key: Constant.eula)
    }
    
// MARK: Direct UserDefault Methods

//******************* SAVE STRING IN USER DEFAULT *******************
    static func saveStringInDefaults(value:String,key:String) {
     UserDefaults.standard.setValue(value, forKey: key);
    }

    //******************* SAVE INT IN USER DEFAULT *******************
    static func saveIntInDefaults(value:Int,key:String) {
     UserDefaults.standard.setValue(value, forKey: key);
    }
    
    //******************* SAVE BOOL IN USER DEFAULT *******************
    static func saveBooleanInDefaults(value:Bool, key:String) {
        UserDefaults.standard.set(value, forKey: key)
    }

    //******************* SAVE DATE IN USER DEFAULT *******************
    static func saveDateInDefaults(value:Date,key:String) {
     UserDefaults.standard.set(value, forKey: key);
    }

    //******************* FETCH STRING FROM USER DEFAULT *******************
    static func fetchStringFromDefaults(key:String) -> String {
        if (UserDefaults.standard.object(forKey: key) != nil) {
            return UserDefaults.standard.string(forKey: key)!;
        }
         else {
            return "" as String;
        }
    }
    
    //******************* FETCH BOOL FROM USER DEFAULT *******************
    
    static func fetchBooleanFromDefaults(key:String) -> Bool {
        if (UserDefaults.standard.object(forKey: key) != nil) {
            return UserDefaults.standard.bool(forKey: key)
        }
        else {
            return false;
        }
    }
    
    
    //******************* FETCH INT FROM USER DEFAULT *******************
    static func fetchIntFromDefaults(key:String) -> Int {
        if (UserDefaults.standard.object(forKey: key) != nil) {
            return UserDefaults.standard.integer(forKey: key);
        }
         else {
            return -1;
        }
    }

    //******************* FETCH DATE FROM USER DEFAULT *******************
    static func fetchDateFromDefaults(key:String) -> Date {
        if (UserDefaults.standard.object(forKey: key) != nil) {
            return (UserDefaults.standard.object(forKey: key) as? Date)!;
        }
         else {
            return Date();
        }
    }
}
