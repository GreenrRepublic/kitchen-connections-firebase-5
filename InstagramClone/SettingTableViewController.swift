//
//  SettingTableViewController.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 2/11/17.
//  Copyright © 2017 The Zero2Launch Team. All rights reserved.
//

import UIKit
import DeallocationChecker

protocol SettingTableViewControllerDelegate {
    func updateUserInfor()
}

class SettingTableViewController: UITableViewController {

    @IBOutlet weak var usernnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    
    var delegate: SettingTableViewControllerDelegate?
    var imagePicker: ImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Edit Profile"
        usernnameTextField.delegate = self
        emailTextField.delegate = self
        imagePicker = ImagePicker(presentationController: self, delegate: self)
        fetchCurrentUser()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        DeallocationChecker.shared.checkDeallocation(of: self)
    }
    func fetchCurrentUser() {
        Api.User.observeCurrentUser { (user) in
            self.usernnameTextField.text = user.username
            self.emailTextField.text = user.email
            
            if user.profileImageURL == nil {
                self.profileImageView.image = UIImage(named: "placeholderImg")
            } else {
                let profileUrl = URL(string: user.profileImageURL!)
                self.profileImageView.sd_setImage(with: profileUrl)
            }
        }
    }
    @IBAction func saveBtn_TouchUpInside(_ sender: Any) {
        if let profileImg = self.profileImageView.image, let imageData = profileImg.jpegData(compressionQuality: 0.1){
            ProgressHUD.show("Waiting...")
            AuthService.updateUserInfor(username: usernnameTextField.text!, email: emailTextField.text!, imageData: imageData, onSuccess: { 
                ProgressHUD.showSuccess("Success")
                self.delegate?.updateUserInfor()
            }, onError: { (errorMessage) in
                ProgressHUD.showError(errorMessage)
            })
        }
    }

    @IBAction func termsBtn_TouchUpInside(_ sender: Any) {
        self.performSegue(withIdentifier: "eulaSegue", sender: nil)
    }
    
    @IBAction func logoutBtn_TouchUpInside(_ sender: Any) {
        AuthService.logout(onSuccess: {
            
            if !Utilities.fetchStringFromDefaults(key: Constant.apple_sign_in_id).isEmpty {
                UserDefaults.standard.set(nil, forKey: Constant.apple_sign_in_id)
            }
            
            let storyboard = UIStoryboard(name: "Start", bundle: nil)
            let signInVC = storyboard.instantiateViewController(withIdentifier: "SignInViewController")
            self.present(signInVC, animated: true, completion: nil)
        }) { (errorMessage) in
            ProgressHUD.showError(errorMessage)
        }
    }
    @IBAction func changeProfileBtn_TouchUpInside(_ sender: Any) {
        self.imagePicker.present(from: sender)
        
//        let pickerController = UIImagePickerController()
//        pickerController.delegate = self
//        pickerController.allowsEditing = false
//        present(pickerController, animated: true, completion: nil)
    }

}

//extension SettingTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//
//    private func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        dismiss(animated: true, completion: nil)
//    }
    
    
//    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        print("did Finish Picking Media")
//        if let image = info[UIImagePickerController.InfoKey.editedImage.rawValue] as? UIImage{
//            profileImageView.image = image
//        }
//        dismiss(animated: true, completion: nil)
//    }
//}

extension SettingTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("return")
        textField.resignFirstResponder()
        return true
    }
}

extension SettingTableViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        self.profileImageView.image = image
    }
}











































