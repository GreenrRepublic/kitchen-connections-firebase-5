//
//  PostApi.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 1/8/17.
//  Copyright © 2017 The Zero2Launch Team. All rights reserved.
//

import Foundation
import FirebaseDatabase
class PostApi {
    var REF_POSTS = Database.database().reference().child("posts")
    func observePosts(completion: @escaping (Post) -> Void) {
        REF_POSTS.observe(.childAdded) { (snapshot: DataSnapshot) in
            if let dict = snapshot.value as? [String: Any] {
                let newPost = Post.transformPostPhoto(dict: dict, key: snapshot.key)
                completion(newPost)
            }
        }
    }
    
    func observePost(withId id: String, completion: @escaping (Post) -> Void) {
        REF_POSTS.child(id).observeSingleEvent(of: DataEventType.value, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let post = Post.transformPostPhoto(dict: dict, key: snapshot.key)
                completion(post)
            }
        })
    }
    
    func observeRecentPosts(completion: @escaping (Post) -> Void) {
        REF_POSTS.queryOrdered(byChild: "createdDate").observeSingleEvent(of: .value, with: {
            snapshot in
            let arraySnapshot = (snapshot.children.allObjects as! [DataSnapshot]).reversed()
            arraySnapshot.forEach({ (child) in
                if let dict = child.value as? [String:Any] {
                    let post = Post.transformPostPhoto(dict: dict, key: child.key)
                    completion(post)
                }
            })
        })
    }
    
    func observeLikeCount(withPostId id: String, completion: @escaping (Int, UInt) -> Void) {
        var likeHandler: UInt!
        likeHandler = REF_POSTS.child(id).observe(.childChanged, with: {
            snapshot in
            if let value = snapshot.value as? Int {
              //  Database.database().reference().removeObserver(withHandle: ref)
                completion(value, likeHandler)
            }
        })
        
    }
    
    func observeTopPosts(completion: @escaping (Post) -> Void) {
        REF_POSTS.queryOrdered(byChild: "likesCount").observeSingleEvent(of: .value, with: {
            snapshot in
            let arraySnapshot = (snapshot.children.allObjects as! [DataSnapshot]).reversed()
            arraySnapshot.forEach({ (child) in
                if let dict = child.value as? [String: Any] {
                    let post = Post.transformPostPhoto(dict: dict, key: child.key)
                    completion(post)
                }
            })
        })
    }
    
    func removeObserveLikeCount(id: String, likeHandler: UInt) {
        Api.Post.REF_POSTS.child(id).removeObserver(withHandle: likeHandler)
    }
    
    func incrementLikes(postId: String, onSuccess: @escaping (Post) -> Void, onError: @escaping (_ errorMessage: String?) -> Void) {
        let postRef = Api.Post.REF_POSTS.child(postId)
        // in posts->post_id:
        postRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var post = currentData.value as? [String : AnyObject], let uid = Api.User.CURRENT_USER?.uid {
                var likes: Dictionary<String, Bool>
                likes = post["likes"] as? [String : Bool] ?? [:]
                var likeCount = post["likesCount"] as? Int ?? 0
                if let _ = likes[uid] {
                    likeCount -= 1
                    likes.removeValue(forKey: uid)
                } else {
                    likeCount += 1
                    likes[uid] = true
                }
                post["likesCount"] = likeCount as AnyObject?
                post["likes"] = likes as AnyObject?
                
                currentData.value = post
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
        }) { (error, committed, snapshot) in
            if let error = error {
                onError(error.localizedDescription)
            }
            if let dict = snapshot?.value as? [String: Any] {
                let post = Post.transformPostPhoto(dict: dict, key: snapshot!.key)
                onSuccess(post)
            }
            // call increment on post? call update to the other field?
        }
    }
    
    func complain(postId:String) {
        let postRef: Void = Api.Post.REF_POSTS.child(postId).child("hasComplain").setValue(true)

    }
    
    
    func incrementLikesOops(postId: String, onSucess: @escaping (Post) -> Void, onError: @escaping (_ errorMessage: String?) -> Void) {
        let postRef = Api.Post.REF_POSTS.child(postId)
        postRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            if var post = currentData.value as? [String : AnyObject], let uid = Api.User.CURRENT_USER?.uid {
                var likes: Dictionary<String, Bool>
                var isToIncrement = true
                likes = post["likes"] as? [String : Bool] ?? [:]
                var likeCount = post["likesCount"] as? Int ?? 0
                if let _ = likes[uid] {
                    isToIncrement = false
                    likeCount -= 1
                    likes.removeValue(forKey: uid)
                } else {
                    isToIncrement = true
                    likeCount += 1
                    likes[uid] = true
                }
                post["likesCount"] = likeCount as AnyObject?
                post["likes"] = likes as AnyObject?
                
                Api.User.REF_USERS.child(uid).observeSingleEvent(of: .value, with: {(snapshot) in
                    let dict = snapshot.value as? [String: Any]
                    var profileLikesCount = dict?["likesCount"] as? Int ?? 0
                    if isToIncrement == true {
                        profileLikesCount+=1
                    } else if isToIncrement == false {
                        profileLikesCount-=1
                    } else {
                        print("error in profile likes incrementer boolean")
                    }
                    //Api.User.REF_USERS.child(uid).setValue(["likesCount" : profileLikesCount])
                    
                })
                
                currentData.value = post
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
        }) { (error, committed, snapshot) in
            if let error = error {
                onError(error.localizedDescription)
            }
            if let dict = snapshot?.value as? [String: Any] {
                let post = Post.transformPostPhoto(dict: dict, key: snapshot!.key)
                print("Post in increment likes function: isLiked:")
                print(post.isLiked)
                onSucess(post)
            }
        }
    }
    
    // Temp
    
    func observeRecentPostsAsArray(completion: @escaping ([Post]) -> Void) {
        var posts = [Post]()
        REF_POSTS.queryOrdered(byChild: "createdDate").observeSingleEvent(of: .value, with: {
            snapshot in
            let arraySnapshot = (snapshot.children.allObjects as! [DataSnapshot]).reversed()
            arraySnapshot.forEach({ (child) in
                if let dict = child.value as? [String:Any] {
                    let post = Post.transformPostPhoto(dict: dict, key: child.key)
                    if (post.hasComplain == false || post.hasComplain == nil) {
                        posts.append(post)
                    } else {
                        // post has complaint, don't show so skip
                    }
                    
                    
                }
            })
            completion(posts)
        })
    }
    
    
    
}
