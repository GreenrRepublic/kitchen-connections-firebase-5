//
//  PeopleViewController.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 1/29/17.
//  Copyright © 2017 The Zero2Launch Team. All rights reserved.
//

import UIKit
import DeallocationChecker

class PeopleViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var users: [UserModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        loadUsers()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        DeallocationChecker.shared.checkDeallocation(of: self)
    }
    func loadUsers() {
        Api.User.observeUsers { (user) in
            self.isFollowing(userId: user.id!, completed: { (value) in
                user.isFollowing = value
            })
            self.isBlocking(userId: user.id!, completed: {(value) in
                user.isBlocking = value
                if user.isBlocking == true {
                    // user blocked so skip
                } else {
                    self.users.append(user)
                    self.tableView.reloadData()
                }

            })
        }
    }
    
    func isFollowing(userId: String, completed: @escaping (Bool) -> Void) {
        Api.Follow.isFollowing(userId: userId, completed: completed)
    }
    
    func isBlocking(userId: String, completed: @escaping (Bool) -> Void) {
        Api.Block.isBlocking(userId: userId, completed: completed)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProfileSegue" {
            let profileVC = segue.destination as! ProfileUserViewController
            let userId = sender as! String
            profileVC.userId = userId
            profileVC.delegate = self
        }
    }
}
extension PeopleViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleTableViewCell", for: indexPath) as! PeopleTableViewCell
        let user = users[indexPath.row]
        cell.user = user
        cell.delegate = self
        return cell
    }
}

extension PeopleViewController: PeopleTableViewCellDelegate {
    func goToProfileUserVC(userId: String) {
        performSegue(withIdentifier: "ProfileSegue", sender: userId)
    }
}

extension PeopleViewController: HeaderProfileCollectionReusableViewDelegate {
    func updateFollowButton(forUser user: UserModel) {
        for u in self.users {
            if u.id == user.id {
                u.isFollowing = user.isFollowing
                self.tableView.reloadData()
            }
        }
    }
    func updateBlockButton(forUser user: UserModel) {
        for u in self.users {
            if u.id == user.id {
                u.isBlocking = user.isBlocked
                self.tableView.reloadData()
            }
        }
    }
}
