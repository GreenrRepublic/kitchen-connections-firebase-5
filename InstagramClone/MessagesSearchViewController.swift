//
//  MessagesSearchViewController.swift
//  KitchenConnectionsFB5
//
//  Created by Garret Kielburger on 2020-08-26.
//  Copyright © 2020 The Zero2Launch Team. All rights reserved.
//

import UIKit
import DeallocationChecker

class MessagesSearchViewController: UIViewController {

    var searchBar = UISearchBar()
    var users: [UserModel] = []

    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.frame.size.width = view.frame.size.width - 60
        
        let searchItem = UIBarButtonItem(customView: searchBar)
        self.navigationItem.rightBarButtonItem = searchItem
        
        doSearch()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        DeallocationChecker.shared.checkDeallocation(of: self)
    }
    
    func doSearch() {
        if let searchText = searchBar.text?.lowercased() {
            self.users.removeAll()
            self.tableView.reloadData()
                Api.User.queryUsers(withText: searchText, completion: { (user) in
                    self.isFollowing(userId: user.id!, completed: { (value) in
                        user.isFollowing = value
                    })
                    self.isBlocking(userId: user.id!, completed: {(value) in
                        user.isBlocking = value
                        if user.isBlocking == true {
                            // user blocked so skip in search
                        } else {
                            self.users.append(user)
                            self.tableView.reloadData()
                        }
                    })
                })
        }
    }
    
    func isFollowing(userId: String, completed: @escaping (Bool) -> Void) {
        Api.Follow.isFollowing(userId: userId, completed: completed)
    }
    
    func isBlocking(userId: String, completed: @escaping (Bool) -> Void) {
        Api.Block.isBlocking(userId: userId, completed: completed)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Search_ProfileSegue" {
            let profileVC = segue.destination as! ProfileUserViewController
            let userId = sender  as! String
            profileVC.userId = userId
            profileVC.delegate = self
        }
//        if segue.identifier == "Search_MessagesSegue" {
////            let chatVC = segue.destination as! ChatViewController
////            let selectedRow = sender as! Int
////            // ChatViewController wants partner image, id, usermode and username
//
//
////            chatVC.partnerId = userId
////            chatVC.partnerUser = user
////            chatVC.partnerUsername = username
////            chatVC.imagePartner = image
//
//        }
    }

}

extension MessagesSearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        doSearch()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        doSearch()
    }
}

extension MessagesSearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessagePeopleTableViewCell", for: indexPath) as!  MessagePeopleTableViewCell
        let user = users[indexPath.row]
        cell.user = user
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("Tapped")
        if let cell = tableView.cellForRow(at: indexPath) as? MessagePeopleTableViewCell {
            let storyboard = UIStoryboard(name: "Messages", bundle: nil)
            let chatVC = storyboard.instantiateViewController(withIdentifier: IDENTIFIER_CHAT) as! ChatViewController
            chatVC.imagePartner = cell.profileImage.image
            chatVC.partnerUsername = cell.nameLabel.text
            chatVC.partnerId = cell.user!.id
            chatVC.partnerUser = cell.user

            self.navigationController?.pushViewController(chatVC, animated: true)
        }
    }
    
    
    
    
    
}
extension MessagesSearchViewController: MessagePeopleTableViewCellDelegate {
    // ChatViewController wants partner image, id, usermode and username
//    func goToMessages(userId: String, userImage: UIImage, userModel: UserModel, username: String) {
//        //performSegue(withIdentifier: "Search_MessagesSegue", sender: userId)
//    }
    
    func goToProfileUserVC(userId: String) {
        performSegue(withIdentifier: "Search_ProfileSegue", sender: userId)
    }
}

extension MessagesSearchViewController: HeaderProfileCollectionReusableViewDelegate {
    func updateFollowButton(forUser user: UserModel) {
//        for u in self.users {
//            if u.id == user.id {
//                u.isFollowing = user.isFollowing
//                self.tableView.reloadData()
//            }
//        }
    }
    func updateBlockButton(forUser user: UserModel) {
//        for u in self.users {
//            if u.id == user.id {
//                u.isBlocking = user.isBlocked
//                self.tableView.reloadData()
//            }
//        }
    }
    
}


