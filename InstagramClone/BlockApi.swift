//
//  BlockApi.swift
//  KitchenConnectionsFB5
//
//  Created by Garret Kielburger on 2021-09-09.
//  Copyright © 2021 The Garret Kielburger Team. All rights reserved.
//

import Foundation
import FirebaseDatabase

class BlockApi {
    

    var REF_BLOCK = Database.database().reference().child("block")
    
    // Collection of who a user is blocked by
    // Blockings are people blocking the user
    var REF_BLOCKING_CROSS_PLATFORM = Database.database().reference().child("block").child(Api.User.CURRENT_USER!.uid).child("blockedBy")
    
    var REF_BLOCKEDBY = Database.database().reference().child("blockedBy")
    
    // Collection of who a user is blocking
    // Blockings are people the user is blocking
    var REF_BLOCKERS_CROSS_PLATFORM = Database.database().reference().child("block").child(Api.User.CURRENT_USER!.uid).child("blockings")
    
    var REF_BLOCKING = Database.database().reference().child("blockings")
    
    
    func blockAction(withUser id: String) {
        
        //REF_BLOCKERS_CROSS_PLATFORM.child(id).child(Api.User.CURRENT_USER!.uid).setValue(true)
        REF_BLOCKEDBY.child(id).child(Api.User.CURRENT_USER!.uid).setValue(true)
        
        //REF_BLOCKING_CROSS_PLATFORM.child(Api.User.CURRENT_USER!.uid).child(id).setValue(true)
        REF_BLOCKING.child(Api.User.CURRENT_USER!.uid).child(id).setValue(true)
        
    }
    
    func unBlockAction(withUser id: String) {
//        REF_BLOCKERS_CROSS_PLATFORM.child(id).child(Api.User.CURRENT_USER!.uid).setValue(NSNull())
        
//        REF_BLOCKING_CROSS_PLATFORM.child(Api.User.CURRENT_USER!.uid).child(id).setValue(NSNull())

        REF_BLOCKEDBY.child(id).child(Api.User.CURRENT_USER!.uid).setValue(NSNull())
        
        REF_BLOCKING.child(Api.User.CURRENT_USER!.uid).child(id).setValue(NSNull())
    }
    
    func isBlocking(userId: String, completed: @escaping (Bool) -> Void) {
        
        REF_BLOCKEDBY.child(userId).child(Api.User.CURRENT_USER!.uid).observeSingleEvent(of: .value, with: {
            snapshot in
            if let _ = snapshot.value as? NSNull {
                completed(false)
            } else {
                completed(true)
            }
        })
    }
    
    func isBlockedBy(userId: String, completed: @escaping (Bool) -> Void) {
        
        REF_BLOCKING.child(Api.User.CURRENT_USER!.uid).child(userId).observeSingleEvent(of: .value, with: {
            snapshot in
            if let _ = snapshot.value as? NSNull{
                completed(false)
            } else {
                completed(true)
            }
        })
        
    }
    
    func allUserBlockings(completed: @escaping ([String]) -> Void) {
        print("allUserBlockings Called")
        var blockingUsers = [String]()
        REF_BLOCKING.child(Api.User.CURRENT_USER!.uid).observe(.value, with: {
            snapshot in
            print("allUserBlockings snapshot: \(snapshot)")
            print("allUserBlockings() snapshot exists: \(snapshot.exists())")
            if !snapshot.exists() {
                print("blockings snapshot doesnt exist")
                //blockingUsers.append("1")
                completed(blockingUsers)
            } else {
                print("Snapshot value key: \(snapshot.key)")
                let blocking = snapshot.key
                    print("Blocking: \(blocking)")
                    blockingUsers.append(blocking)
                completed(blockingUsers)
            }
        })
    }
    
    func allUserBlockedBy(completed: @escaping ([String]) -> Void) {
        var blockedByUsers = [String]()
        let currentUser = Api.User.CURRENT_USER!.uid
        REF_BLOCKEDBY.queryOrderedByKey().observeSingleEvent(of: .value, with: {
            snapshot in
            if !snapshot.exists() {
                //print("blockedby snapshot doesnt exist")
                blockedByUsers.append("1")
                completed(blockedByUsers)
            } else {
                snapshot.children.forEach({
                    (child) in
                    let blockedUser = child as! DataSnapshot
                    blockedUser.children.forEach({
                        (grandchild) in
                        let blockedBy = grandchild as! DataSnapshot
                        //print("Blocked User: \(blockedUser.key)")
                        //print("User Blocked By: \(blockedBy.key)")
                        
                        if currentUser == blockedBy.key {
                            blockedByUsers.append(blockedUser.key)
                        }
                    })
                })
                completed(blockedByUsers)
            }
            
        })
    }
    
    
}
