//
//  UserApi.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 1/8/17.
//  Copyright © 2017 The Zero2Launch Team. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth
class UserApi {
    var REF_USERS = Database.database().reference().child("profiles")
    
    func observeUserByUsername(username: String, completion: @escaping (UserModel) -> Void) {
        REF_USERS.queryOrdered(byChild: "username_lowercase").queryEqual(toValue: username).observeSingleEvent(of: .childAdded, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let user = UserModel.transformUser(dict: dict, key: snapshot.key)
                completion(user)
            }
        })
    }
    
    func observeUser(withId id: String, completion: @escaping (UserModel) -> Void) {
        REF_USERS.child(id).observeSingleEvent(of: .value, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let user = UserModel.transformUser(dict: dict, key: snapshot.key)
                completion(user)
            }
        })
    }
    
    func observeCurrentUser(completion: @escaping (UserModel) -> Void) {
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        REF_USERS.child(currentUser.uid).observeSingleEvent(of: .value, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let user = UserModel.transformUser(dict: dict, key: snapshot.key)
                completion(user)
            }
        })
    }
    
    func observeUsers(completion: @escaping (UserModel) -> Void) {
        REF_USERS.observe(.childAdded, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let user = UserModel.transformUser(dict: dict, key: snapshot.key)
                completion(user)
            }
        })
    }
    
    func queryUsers(withText text: String, completion: @escaping (UserModel) -> Void) {
        REF_USERS.queryOrdered(byChild: "username_lowercase").queryStarting(atValue: text).queryEnding(atValue: text+"\u{f8ff}").queryLimited(toFirst: 10).observeSingleEvent(of: .value, with: {
            snapshot in
            snapshot.children.forEach({ (s) in
                let child = s as! DataSnapshot
                if let dict = child.value as? [String: Any] {
                    let user = UserModel.transformUser(dict: dict, key: child.key)
                    completion(user)
                }
            })
        })
    }
    
    var CURRENT_USER: User? {
        if let currentUser = Auth.auth().currentUser {
            return currentUser
        }
        
        return nil
    }
    
    var REF_CURRENT_USER: DatabaseReference? {
        guard let currentUser = Auth.auth().currentUser else {
            return nil
        }
        
        return REF_USERS.child(currentUser.uid)
    }
    
    
    var currentUserId: String {
        return Auth.auth().currentUser != nil ? Auth.auth().currentUser!.uid : ""
    }
    
    func isOnline(bool: Bool) {
        if !Api.User.currentUserId.isEmpty {
            let ref = Ref().databaseIsOnline(id: Api.User.currentUserId)
            let dict: Dictionary<String, Any> = [
                "online": bool as Any,
                "latest": Date().timeIntervalSince1970 as Any
            ]
            ref.updateChildValues(dict)
        }
    }
    
    func typing(from: String, to: String) {
        let ref = Ref().databaseIsOnline(id: from)
        let dict: Dictionary<String, Any> = [
            "typing": to
        ]
        ref.updateChildValues(dict)
    }
    
    func getUserInfor(id: String, onSuccess: @escaping(UserCompletion)) {
        let ref = Ref().databaseSpecificUser(id: id)
        print("ref for user:")
        print(ref)
        ref.observe(.value) { (snapshot) in
            if let dict = snapshot.value as? Dictionary<String, Any> {
                print("dict from fb for user:")
                print(dict)
                if let user = UserModel.transformUserChat(dict: dict) {
                    print("user from fb: ")
                    print(user)
                    onSuccess(user)
                }
            }
        }
    }
    
    func getUserInforSingleEvent(id: String, onSuccess: @escaping(UserCompletion)) {
        let ref = Ref().databaseSpecificUser(id: id)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? Dictionary<String, Any> {
                if let user = UserModel.transformUserChat(dict: dict) {
                    onSuccess(user)
                }
            }
        }
    }
    
    // Temp
    
    func observeUsersAsArray(withPosts posts: [Post], completion: @escaping ([UserModel]) -> Void) {
        var profiles = [UserModel]()
        //var sortedProfiles = [UserModel]()
        for post in posts {
            //print("authorId -> \(post.authorId!)")
            REF_USERS.child(post.authorId!).observeSingleEvent(of: .value, with: {
                snapshot in
                if let dict = snapshot.value as? [String: Any] {
                    let user = UserModel.transformUser(dict: dict, key: snapshot.key)
                    profiles.append(user)
                    //print("username -> \(user.username!)")
                }
                //print("profiles -> \(profiles)")
                completion(profiles)
            })
        }
        
    }
    
}
typealias UserCompletion = (UserModel) -> Void
