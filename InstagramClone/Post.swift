//
//  Post.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 12/26/16.
//  Copyright © 2016 The Zero2Launch Team. All rights reserved.
//

import Foundation
import FirebaseAuth
class Post {
    var caption: String?       // visible
    var imageURL: String?
    var imageTitle: String?       // visible
    
    var authorId: String?       // visible
    
    // Counts
    var commentsCount: Int?       // visible
    var likesCount: Int?        // visible
    var watchersCount: Int?        // visible
    
    //var isLiked: Bool?
    
    var hasComplain: Bool?
    
    // need itemType from Android?
    
    var latitude: Double?
    var longitude: Double?
    var location: String?       // visible
    var city: String?       // visible
    
    // what is this needed for?
    //var uid: String? -> used by iOS instagramClone as authorId
    var ratio: CGFloat?
    var videoUrl: String?
    //var likes: Dictionary<String, Any>?
    
    var id: String?
    var title: String?       // visible
    var description: String?       // visible
    
    var createdDate: Int?
    var createdDateText: String?       // visible
    
    // for handling likes within post instead of in separate object
    var likes: Dictionary<String, Any>?
    var isLiked: Bool?
}

extension Post {
    static func transformPostPhoto(dict: [String: Any], key: String) -> Post {
        let post = Post()
        
        post.id = key
        post.title = dict["title"] as? String
        post.authorId = dict["authorId"] as? String
        post.city = dict["city"] as? String
        post.commentsCount = dict["commentsCount"] as? Int
        post.createdDate = dict["createdDate"] as? Int
        post.createdDateText = dict["createdDateText"] as? String
        post.description = dict["description"] as? String
        post.caption = dict["description"] as? String
        post.hasComplain = dict["hasComplain"] as? Bool
        post.imageURL = dict["imagePath"] as? String
        post.imageTitle = dict["imageTitle"] as? String
        post.latitude = dict["latitude"] as? Double
        post.longitude = dict["longitude"] as? Double
        post.location = dict["location"] as? String
        post.watchersCount = dict["watchersCount"] as? Int
        post.likesCount = dict["likesCount"] as? Int
        
        post.ratio = dict["ratio"] as? CGFloat
        
        // way of handling likes
        post.likes = dict["likes"] as? Dictionary<String, Any>
        
        if post.likesCount == nil {
            post.likesCount = 0
        }
        if let currentUserId = Auth.auth().currentUser?.uid {
            if post.likes != nil {
                post.isLiked = post.likes![currentUserId] != nil
            }
        }

        return post

    }
    
    static func transformPostVideo() {
        
    }
}
