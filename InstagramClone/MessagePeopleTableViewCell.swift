//
//  MessagePeopleTableViewCell.swift
//  KitchenConnectionsFB5
//
//  Created by Garret Kielburger on 2020-08-26.
//  Copyright © 2020 The Zero2Launch Team. All rights reserved.
//
import UIKit
protocol MessagePeopleTableViewCellDelegate {
    func goToProfileUserVC(userId: String)
    //func goToMessages(userId: String, userImage: UIImage, userModel: UserModel, username: String)
}
class MessagePeopleTableViewCell: UITableViewCell {

    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
 //   @IBOutlet weak var messageButton: UIButton!
    
    
    var delegate: MessagePeopleTableViewCellDelegate?
    var user: UserModel? {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        nameLabel.text = user?.username
        if let photoUrlString = user?.profileImageURL {
            let photoUrl = URL(string: photoUrlString)
            profileImage.sd_setImage(with: photoUrl, placeholderImage: UIImage(named: "placeholderImg"))
            
        }
        

        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.nameLabel_TouchUpInside))
        nameLabel.addGestureRecognizer(tapGesture)
        nameLabel.isUserInteractionEnabled = true
    }
    
    @objc func nameLabel_TouchUpInside() {
        if let id = user?.id {
            delegate?.goToProfileUserVC(userId: id)
        }
    }
    
//    @objc func messageButton_TouchUpInside(){
////        if let id = user?.id {
////            // ChatViewController wants partner image, id, usermode and username
////            delegate?.goToMessages(userId: id, userImage: profileImage.image!, userModel: user!, username: user!.username!)
////        }
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
