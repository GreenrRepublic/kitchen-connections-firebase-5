//
//  User.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 12/30/16.
//  Copyright © 2016 The Zero2Launch Team. All rights reserved.
//

import Foundation
class UserModel {
    var email: String?
    var profileImageURL: String?
    var username: String?
    var id: String?
    var likesCount: Int?

    var isFollowing: Bool?
    var isBlocking: Bool?
    var isBlocked: Bool?
    
    func updateData(key: String, value: String) {
        switch key {
        case "username": self.username = value
        case "email": self.email = value
        case "photoUrl": self.profileImageURL = value
        default: break
        }
    }
}

extension UserModel {
    static func transformUser(dict: [String: Any], key: String) -> UserModel {
        let user = UserModel()
        user.email = dict["email"] as? String
        user.likesCount = dict["likesCount"] as? Int
        if let profileImageUrl = dict["photoUrl"] as? String {
            user.profileImageURL = dict["photoUrl"] as? String
        }
        user.username = dict["username"] as? String
        user.id = key
        return user
    }
}

extension UserModel {
    static func transformUserChat(dict: [String: Any]) -> UserModel? {
        let user = UserModel()
        guard let email = dict["email"] as? String,
            let username = dict["username"] as? String,
            let profileImageURL = dict["photoUrl"] as? String,
            let id = dict["id"] as? String else {
                return nil
        }
        user.email = email
        user.username = username
        user.profileImageURL = profileImageURL
        user.id = id

        return user
    }
}
