//
//  CameraViewController.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 12/4/16.
//  Copyright © 2016 The Zero2Launch Team. All rights reserved.
//

import UIKit
import AVFoundation
import ImagePicker
import DeallocationChecker

class CameraViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var captionTextView: UITextView!
    @IBOutlet weak var removeButton: UIBarButtonItem!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var titleTextField: UITextField!
    
    var selectedImage: UIImage?
    var videoUrl: URL?
    var imagePicker: ImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleSelectPhoto))
        photo.addGestureRecognizer(tapGesture)
        photo.isUserInteractionEnabled = true
        
        captionTextView.layer.borderWidth = 1.0
        captionTextView.layer.borderColor = UIColor.gray.cgColor
        
        captionTextView.textColor = .systemGray2
        captionTextView.text = "Enter a description"
        captionTextView.delegate = self
        
        titleTextField.layer.borderWidth = 1.0
        titleTextField.layer.borderColor = UIColor.gray.cgColor
        titleTextField.setLeftPadding()
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DeallocationChecker.shared.checkDeallocation(of: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handlePost()
    }
    
    func handlePost() {
        print("handlePost Called. selectedImage: \(selectedImage)")
        if selectedImage != nil {
           self.shareButton.isEnabled = true
           self.removeButton.isEnabled = true
            //self.shareButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        } else {
           self.shareButton.isEnabled = false
            self.removeButton.isEnabled = false
            //self.shareButton.backgroundColor = .lightGray

        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func handleSelectPhoto(sender: UITapGestureRecognizer) {
        let photoView = sender.view as! UIImageView
        self.imagePicker.present(from: sender)

    }
    @IBAction func shareButton_TouchUpInside(_ sender: Any) {
        print("ShareButton Clicked")
        view.endEditing(true)
        ProgressHUD.show("Waiting...", interaction: false)
        if let profileImg = self.selectedImage, let imageData = profileImg.jpegData(compressionQuality: 0.1){
            var ratio = profileImg.size.width / profileImg.size.height
            if ratio > 1 {
                ratio = 1
            }
            HelperService.uploadDataToServer(data: imageData, videoUrl: self.videoUrl, ratio: ratio, caption: captionTextView.text!, title: titleTextField.text!, onSuccess: {
            self.clean()
            self.tabBarController?.selectedIndex = 0
        })
        
        } else {
            ProgressHUD.showError("Profile Image can't be empty")
        }
    }
//    @IBAction func cameraButton_TouchUpInside(_ sender: Any) {
//        let imagePickerController = ImagePickerController()
//        imagePickerController.delegate = self
//        imagePickerController.imageLimit = 1
//        present(imagePickerController, animated: true, completion: nil)
//    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard let image = images.first else {
            dismiss(animated: true, completion: nil)
            return
        }
        selectedImage = image
        photo.image = image
        dismiss(animated: true, completion: {
            self.performSegue(withIdentifier: "filter_segue", sender: nil)
        })
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        print("cancel")
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .systemGray2 {
            textView.text = nil
            textView.textColor = .label
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter a description"
            textView.textColor = .systemGray2
        }
    }
    
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//
//        let currentText:String = textView.text
//        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
//
//        if updatedText.isEmpty {
//            textView.text = "Enter a description"
//            textView.textColor = UIColor.lightGray
//            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
//        } else if !text.isEmpty {
//            textView.textColor = UIColor.black
//            textView.text = text
//        } else {
//            return true
//        }
//        return false
//    }
    
    @IBAction func remove_TouchUpInside(_ sender: Any) {
        clean()
        handlePost()
    }
    
    func clean() {
        self.captionTextView.text = ""
        self.photo.image = UIImage(named: "placeholder-photo")
        self.selectedImage = nil
        self.titleTextField.text = ""
        //self.titleTextField.placeholder = "Enter a title"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filter_segue" {
            let filterVC = segue.destination as! FilterViewController
            filterVC.selectedImage = self.selectedImage
            filterVC.delegate = self
        }
    }
    
}
//extension CameraViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        print("did Finish Picking Media")
//        print(info)
//
//        if let videoUrl = info["UIImagePickerControllerMediaURL"] as? URL {
//            if let thumnailImage = self.thumbnailImageForFileUrl(videoUrl) {
//                selectedImage = thumnailImage
//                photo.image = thumnailImage
//                self.videoUrl = videoUrl
//            }
//            dismiss(animated: true, completion: nil)
//        }
//
//        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage{
//            selectedImage = image
//            photo.image = image
//            dismiss(animated: true, completion: {
//                self.performSegue(withIdentifier: "filter_segue", sender: nil)
//            })
//        }
//    }
//
//    func thumbnailImageForFileUrl(_ fileUrl: URL) -> UIImage? {
//        let asset = AVAsset(url: fileUrl)
//        let imageGenerator = AVAssetImageGenerator(asset: asset)
//        imageGenerator.appliesPreferredTrackTransform = true
//
//        do {
//
//            let thumbnailCGImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 7, timescale: 1), actualTime: nil)
//            return UIImage(cgImage: thumbnailCGImage)
//        } catch let err {
//            print(err)
//        }
//
//        return nil
//    }
//}

extension CameraViewController: FilterViewControllerDelegate {
    func updatePhoto(image: UIImage) {
        self.photo.image = image
        self.selectedImage = image
    }
}

extension CameraViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        self.photo.image = image
        self.selectedImage = image
        self.handlePost()
    }
}

extension UITextField {

    func setLeftPadding(_ amount: CGFloat = 5) {

        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.bounds.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }

    func setRightPadding(_ amount: CGFloat = 5) {

        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.bounds.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
