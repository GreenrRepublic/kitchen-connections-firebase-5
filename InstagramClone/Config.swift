//
//  Config.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 12/17/16.
//  Copyright © 2016 The Zero2Launch Team. All rights reserved.
//

import Foundation

struct Config {
    static var STORAGE_ROOF_REF = "gs://kitchen-connections.appspot.com"
}


let serverKey = "AAAA61axsbo:APA91bFqfmt9unpv4Ns5Fyse39jNQOPeHTH5nPifUZ4DD-AC4USzxauruFA5ZVBoQzkjI1FSaTFMBe7hHpxDeN_j48lN8jb9og_4L0mnigs6Iz4ow1Wbd-PHu1QEvYAnfU1uDdxjJ1aa"
let fcmUrl = "https://fcm.googleapis.com/fcm/send"

func sendRequestNotification(isMatch: Bool = false, fromUser: UserModel, toUser: UserModel, message: String, badge: Int) {
    var request = URLRequest(url: URL(string: fcmUrl)!)
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("key=\(serverKey)", forHTTPHeaderField: "Authorization")
    request.httpMethod = "POST"
    
    let notification: [String: Any] = [ "to" : "/topics/\(toUser.id)",
        "notification" : ["title": (isMatch == false) ? fromUser.username : "New Match",
                          "body": message,
                          "sound" : "default",
                          "badge": badge,
                          "customData" : ["userId": fromUser.id,
                                          "username": fromUser.username,
                                          "email": fromUser.email,
                                          "photoUrl": fromUser.profileImageURL]
        ]
    ]
    
    let data = try! JSONSerialization.data(withJSONObject: notification, options: [])
    request.httpBody = data
    
    let session = URLSession.shared
    session.dataTask(with: request) { (data, response, error) in
        guard let data = data, error == nil else {
            return
        }
        
        if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
            print("HttpUrlResponse \(httpResponse.statusCode)")
            print("Response \(response!)")
        }
        
        if let responseString = String(data: data, encoding: String.Encoding.utf8) {
            print("ResponseString \(responseString)")
        }
        }.resume()
    
}
