//
//  SignInViewController.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 11/30/16.
//  Copyright © 2016 The Zero2Launch Team. All rights reserved.
//

import UIKit
import GoogleSignIn
import AuthenticationServices
import CryptoKit
import FirebaseAuth
import FirebaseDatabase
import CommonCrypto

class SignInViewController: UIViewController {
    

    @IBOutlet weak var googleAppleStackView: UIStackView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var googleSignIn: GIDSignInButton!
    let appleSignInButton = ASAuthorizationAppleIDButton(type: .default, style: .whiteOutline)
    fileprivate var currentNonce: String?
    
    
    @IBAction func eulaButtonClick(_ sender: Any) {
        self.performSegue(withIdentifier: "eulaSegue", sender: nil)
    }
    
    @IBAction func eulaSwitchChanged(_ sender: UISwitch) {
        Utilities.saveEULA(bool: sender.isOn)
        enableButtons(onOff: sender.isOn)

    }
    
    @IBOutlet weak var eulaSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        emailTextField.backgroundColor = UIColor.clear
//        emailTextField.tintColor = UIColor.white
//        emailTextField.textColor = UIColor.white
//        emailTextField.attributedPlaceholder = NSAttributedString(string: emailTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)])
//        let bottomLayerEmail = CALayer()
//        bottomLayerEmail.frame = CGRect(x: 0, y: 29, width: 1000, height: 0.6)
//        bottomLayerEmail.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 25/255, alpha: 1).cgColor
//        emailTextField.layer.addSublayer(bottomLayerEmail)
//
//        passwordTextField.backgroundColor = UIColor.clear
//        passwordTextField.tintColor = UIColor.white
//        passwordTextField.textColor = UIColor.white
//        passwordTextField.attributedPlaceholder = NSAttributedString(string: passwordTextField.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor(white: 1.0, alpha: 0.6)])
//        let bottomLayerPassword = CALayer()
//        bottomLayerPassword.frame = CGRect(x: 0, y: 29, width: 1000, height: 0.6)
//        bottomLayerPassword.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 25/255, alpha: 1).cgColor
//        passwordTextField.layer.addSublayer(bottomLayerPassword)
        signInButton.isEnabled = false
        handleTextField()
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        if traitCollection.userInterfaceStyle == .dark {
            googleSignIn.colorScheme = .dark
            appleSignInButton.overrideUserInterfaceStyle.self = .light
        } else {
            googleSignIn.colorScheme = .light
            appleSignInButton.overrideUserInterfaceStyle.self = .dark
        }
        
        googleSignIn.style = .wide
        //googleSignIn.setTitle("Sign in with Google", for: UIControl.State.normal)
        //googleSignIn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        //googleSignIn.backgroundColor = UIColor(red: 223/255, green: 74/255, blue: 50/255, alpha: 1)
        googleSignIn.layer.cornerRadius = 5
        googleSignIn.clipsToBounds = true
        
        
        //googleSignIn.setImage(UIImage(named: "icon-google") , for: UIControl.State.normal)
        //googleSignIn.imageView?.contentMode = .scaleAspectFit
        googleSignIn.tintColor = .white
        //googleSignIn.imageEdgeInsets = UIEdgeInsets(top: 12, left: -35, bottom: 12, right: 0)
        
        
        eulaSwitch.setOn(Utilities.fetchEULA(), animated: true)
        googleSignIn.isEnabled = Utilities.fetchEULA()
        
        
        //appleSignInButton.translatesAutoresizingMaskIntoConstraints = false
        appleSignInButton.addTarget(self, action: #selector(appleSignInButtonTapped), for: .touchUpInside)
        googleAppleStackView.addArrangedSubview(appleSignInButton)
        
        //checkForPreviousAppleLogin()
        
//        let horizontalConstraint = NSLayoutConstraint(item: appleButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
//
//        let verticalConstraint = NSLayoutConstraint(item: googleSignIn, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: .equal, toItem: appleButton, attribute: .top, multiplier: 1, constant: 25)
//
//        let widthConstraint = NSLayoutConstraint(item: appleButton, attribute: .width, relatedBy: .equal, toItem: googleSignIn, attribute: .width, multiplier: 1, constant: 0)
//
//        let heightConstaint = NSLayoutConstraint(item: appleButton, attribute: .height, relatedBy: .equal, toItem: googleSignIn, attribute: .height, multiplier: 1, constant: 0)

        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let defaults = UserDefaults.standard
        let hasViewedWalkthrough = defaults.bool(forKey: "hasViewedWalkthrough")
        let hasAgreedTerms = defaults.bool(forKey: "EULA")
        
        let eulaClosure = {
             if !hasAgreedTerms {
                if let eulaVC = self.storyboard?.instantiateViewController(withIdentifier: "EulaViewController") as? EulaViewController {
                    self.present(eulaVC, animated: true, completion: nil)
                }
            }
        }
        
        if !hasViewedWalkthrough {
            if let pageVC = storyboard?.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController {
                present(pageVC, animated: true, completion: eulaClosure)
            }
        }
     
        
        if Api.User.CURRENT_USER != nil {
            self.performSegue(withIdentifier: "signInToTabbarVC", sender: nil)
            
        }
    }
    
    func handleTextField() {
        emailTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: UIControl.Event.editingChanged)
        passwordTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: UIControl.Event.editingChanged)
        
    }
    
    func enableButtons(onOff: Bool) {
        googleSignIn.isEnabled = onOff
        appleSignInButton.isEnabled = onOff
        signInButton.isEnabled = onOff
    }
    
    @objc func textFieldDidChange() {
        guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty else {
                signInButton.setTitleColor(UIColor.lightText, for: UIControl.State.normal)
                signInButton.isEnabled = false
                return
        }
        if Utilities.fetchEULA() {
            signInButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
            signInButton.isEnabled = true
        }
    }
    
    @IBAction func signInButton_TouchUpInside(_ sender: Any) {
        view.endEditing(true)
        ProgressHUD.show("Waiting...", interaction: false)
        AuthService.signIn(email: emailTextField.text!, password: passwordTextField.text!, onSuccess: {
            ProgressHUD.showSuccess("Success")
            self.performSegue(withIdentifier: "signInToTabbarVC", sender: nil)
            
        }, onError: { error in
            ProgressHUD.showError(error!)
        })
    }
    
    
    //MARK: Apple SignIn Methods
    
    @objc func appleSignInButtonTapped() {
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        // Generate nonce for validation after authentication successful
        currentNonce = randomNonceString()
        // Set the SHA256 hashed nonce to ASAuthorizationAppleIDRequest
        request.nonce = currentNonce?.sha256()
        
        
        let authController = ASAuthorizationController(authorizationRequests: [request])
        authController.delegate = self
        authController.presentationContextProvider = self
        authController.performRequests()
        
    }
}

extension SignInViewController: ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

extension SignInViewController: ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("authorization error")
        guard let error = error as? ASAuthorizationError else {
            return
        }

        switch error.code {
        case .canceled:
            // user press "cancel" during the login prompt
            print("Canceled")
        case .unknown:
            // user didn't login their Apple ID on the device
            print("Unknown")
        case .invalidResponse:
            // invalid response received from the login
            print("Invalid Respone")
        case .notHandled:
            // authorization request not handled, maybe internet failure during login
            print("Not handled")
        case .failed:
            // authorization failed
            print("Failed")
        @unknown default:
            print("Default")
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            let nickname = fullName?.nickname
            
            self.saveUserInKeychain(userIdentifier)
            Utilities.saveStringInDefaults(value: userIdentifier, key: Constant.apple_sign_in_id)
            
            var identityToken : String?
            if let token = appleIDCredential.identityToken {
                identityToken = String(bytes: token, encoding: .utf8)
            }

            var authorizationCode : String?
            if let code = appleIDCredential.authorizationCode {
                authorizationCode = String(bytes: code, encoding: .utf8)
            }
            
            // Login to Firebase
        
            // Retrieve the secure nonce generated during Apple sign in
            //let nonce = currentNonce
            
            let firebaseCredential = OAuthProvider.credential(withProviderID: "apple.com", idToken: identityToken!, rawNonce: currentNonce)
            
            Auth.auth().signIn(with: firebaseCredential, completion: { [weak self] (authResult, error) in
                if let error = error {
                    print("Error in FB Auth: \(error)");
                    return
                }
                
                let changeRequest = authResult?.user.createProfileChangeRequest()
                changeRequest?.displayName = fullName?.givenName
                changeRequest?.commitChanges(completion: { (error) in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        print("updated display name")
                        
                        // Firebase is signed in
                        var databaseRef = Database.database().reference()
                        databaseRef.child("profiles").child(authResult!.user.uid).observeSingleEvent(of: .value, with: {
                            (snapshot) in
                            let snapshot = snapshot.value as? NSDictionary

                            if (snapshot == nil) {


                                databaseRef.child("profiles").child(authResult!.user.uid).child("username").setValue(fullName?.givenName)
                                databaseRef.child("profiles").child(authResult!.user.uid).child("email").setValue(authResult?.user.email)
                                databaseRef.child("profiles").child(authResult!.user.uid).child("id").setValue(authResult?.user.uid)
                                databaseRef.child("profiles").child(authResult!.user.uid).child("username_lowercase").setValue(authResult?.user.displayName?.lowercased())
                                
                            }

                        })
                        
                    }
                })
                
                self!.performSegue(withIdentifier: "signInToTabbarVC", sender: nil)
                
                
            })
            
            
        
        case let passwordCredential as ASPasswordCredential:
        
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            
        default:
            break
        }
    }
    
    private func saveUserInKeychain(_ userIdentifier: String) {
        do {
            try KeychainItem(service: "com.greenrrepublic.kitchenconnections.key", account: "userIdentifier").saveItem(userIdentifier)
        } catch {
            print("Unable to save userIdentifier to keychain.")
        }
    }

    
    
    // MARK: Firebase for Apple Sign In
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }

    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
    
    
}

extension String {

func sha256() -> String {
    if let stringData = self.data(using: String.Encoding.utf8) {
        return hexStringFromData(input: digest(input: stringData as NSData))
    }
    return ""
}

private func digest(input : NSData) -> NSData {
    let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
    var hash = [UInt8](repeating: 0, count: digestLength)
    CC_SHA256(input.bytes, UInt32(input.length), &hash)
    return NSData(bytes: hash, length: digestLength)
}

private func hexStringFromData(input: NSData) -> String {
    var bytes = [UInt8](repeating: 0, count: input.length)
    input.getBytes(&bytes, length: input.length)

    var hexString = ""
    for byte in bytes {
        hexString += String(format:"%02x", UInt8(byte))
    }

    return hexString
}

}
