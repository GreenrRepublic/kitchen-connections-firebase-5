//
//  MapViewController.swift
//  KitchenConnectionsFB5
//
//  Created by Captain on 2018-12-31.
//  Copyright © 2018 The Zero2Launch Team. All rights reserved.
//

import Foundation
import MapKit
import DeallocationChecker

class MapViewController: UIViewController {
    
    // MARK: Parameters
    @IBOutlet weak var mapView: MKMapView!
    
    let regionRadius: CLLocationDistance = 25000 // meters
    var posts: [Post] = []
    var annotations: [PostAnnotation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: NSStringFromClass(PostAnnotation.self))
        
        // Initial Location will be user's location - if no location, then Ottawa :)
        let preferences = UserDefaults.standard
        let latKey = "locationLat"
        let lonKey = "locationLon"
        
        let userLat: Double? = preferences.double(forKey: latKey)
        let userLon: Double? = preferences.double(forKey: lonKey)
        
        var initialLocation: CLLocation
        
        if (userLat != nil) && (userLon != nil) {
            initialLocation = CLLocation(latitude: userLat!, longitude: userLon!)
        } else {
            // default to Ottawa :)
            initialLocation = CLLocation(latitude: 45.4215, longitude: -75.6972)
        }
        
        centerMapOnLocation(location: initialLocation)
        
        // Get the posts and append data
        loadPostData()
        checkLocationAuthorizationStatus()
    }
    
    func loadPostData(){
        self.posts.removeAll()
        Api.Post.observePosts { (post) in
            //self.posts.append(post)
            //print(self.posts.count)
            
            // turn the posts into map annotations
            if (post.latitude == nil) {
                return
            }
            
            let annotation = PostAnnotation()
            annotation.coordinate = CLLocationCoordinate2DMake(post.latitude!, post.longitude!)
            annotation.title = post.title
            annotation.subtitle = post.caption
            annotation.setImageUrl(imageURL: post.imageURL!)
            annotation.postId = post.id
            self.annotations.append(annotation)
            
            //add annotations to map
            self.mapView.addAnnotation(annotation)
        }
        
        //convertPostData()
    }
    
    func convertPostData() {
        
        for post in posts {
            print("posts count:")
            print(posts.count)
            let annotation = PostAnnotation()
            annotation.coordinate = CLLocationCoordinate2DMake(post.latitude!, post.longitude!)
            annotation.title = post.title
            annotation.subtitle = post.caption
            annotation.setImageUrl(imageURL: post.imageURL!)
            annotation.postId = post.id
            //annotation.canShowCallout = true
            //annotation.animatesWhenAdded = true
            
            annotations.append(annotation)
        }
            print("annotations count:")
            print(annotations.count)
            mapView.addAnnotations(annotations)
        
    }
    
    // MARK: - CLLocationManager
    
    let locationManager = CLLocationManager()
    func checkLocationAuthorizationStatus() {
        if (CLLocationManager.authorizationStatus() == .authorizedAlways) || (CLLocationManager.authorizationStatus() == .authorizedWhenInUse) {
            mapView.showsUserLocation = true
            print(locationManager.location)
            
        } else {
            locationManager.requestAlwaysAuthorization()
        }
        //    if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
        //      mapView.showsUserLocation = true
        //    } else {
        //      locationManager.requestWhenInUseAuthorization()
        //    }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Map_DetailSegue" {
            let detailVC = segue.destination as! DetailViewController
            let postId = sender  as! String
            detailVC.postId = postId
        }
    }
    
}
extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        print("mapView viewFor called")
        guard !annotation.isKind(of: MKUserLocation.self) else {
            // Make a fast exit if the annotation is the `MKUserLocation`, as it's not an annotation view we wish to customize.
            return nil
        }
        var annotationView: MKAnnotationView?
        
        if let annotation = annotation as? PostAnnotation {
            print("viewFor selecting proper annotationView")
            annotationView = setupPostAnnotation(for: annotation, on: mapView)
        }
        return annotationView
    }
    
    /// Called when the user taps the post image in the callout.
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
         print("Tapped Post annotation accessory view")
        // Detect which annotation type was tapped on for its callout - might have more than one type eventually
        if let annotation = view.annotation, annotation.isKind(of: PostAnnotation.self) {
            
            let postAnnotation = annotation as! PostAnnotation
            let postId = postAnnotation.postId
            performSegue(withIdentifier: "Map_DetailSegue", sender: postId)
   
        }
    }
 
    private func setupPostAnnotation(for annotation: PostAnnotation, on mapView: MKMapView) -> MKAnnotationView {
        let identifier = NSStringFromClass(PostAnnotation.self)
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier, for: annotation)
        if let markerAnnotationView = view as? MKMarkerAnnotationView {
            markerAnnotationView.animatesWhenAdded = true
            markerAnnotationView.canShowCallout = true
            markerAnnotationView.markerTintColor = UIColor.purple
            
            // Provide an image view to use as the accessory view's detail view.
            //let photoUrl = URL(string: "https://firebasestorage.googleapis.com/v0/b/kitchen-connections.appspot.com/o/images%2Fpost_-LSc5erqT5cqWcGcLVQC?alt=media&token=e8660114-9d6f-4bae-b41e-48e0f8c0c0b3")
            //var image1: UIImageView = UIImageView(image: (resourceName: "placeholder-photo"))
            
            let photoUrl = URL(string: annotation.photoURL!)
            let image2 = UIImageView(image: #imageLiteral(resourceName: "placeholderImg"))
            image2.sd_setImage(with: photoUrl)
            let heightConstraint = NSLayoutConstraint(item: image2, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.lessThanOrEqual, toItem: image2, attribute: NSLayoutConstraint.Attribute.width, multiplier: 0.6, constant: 50)
            let widthConstraint = NSLayoutConstraint(item: image2, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.lessThanOrEqual, toItem: image2, attribute: NSLayoutConstraint.Attribute.height, multiplier: 0.6, constant: 50)
            image2.addConstraints([heightConstraint, widthConstraint])
            
            markerAnnotationView.detailCalloutAccessoryView = image2

            let rightButton = UIButton(type: .detailDisclosure)
//            let emptyView = UIView()
  //          emptyView.draw(CGRect.init(x: 5, y: 5, width: 1, height: 1))
            rightButton.frame(forAlignmentRect: CGRect.init(x: 0, y: 0, width: 1, height: 1))
            markerAnnotationView.rightCalloutAccessoryView = rightButton
            
        }
        
        return view
    }
    
}
